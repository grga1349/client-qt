#include "stest.h"

sTest::sTest(QObject *parent) :
    QThread(parent)
{
    buff="";
    TRun=false;
}

bool sTest::Connect(QString cnt,int s)
{
   socket=new QTcpSocket(this);
   socket->connectToHost(cnt,s);
   if(socket->waitForConnected(1000))
       {
       TRun=true;
       return true;
       }
   else
       return false;
}

void sTest::Send(const QByteArray &wrt)
{
    socket->write(wrt);
}

void sTest::run()
{

    QMutex m;
    m.lock();
    while(TRun)
       {
       m.unlock();
       this->msleep(100);
       m.lock();
       buff=socket->readAll();
       if (buff!="")
       emit Recv(20);
       }
    m.unlock();
}

void sTest::Disconnect()
{
    socket->disconnectFromHost();
    TRun=false;
}










