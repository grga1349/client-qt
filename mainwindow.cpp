#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    st=new sTest(this);
    connect(st,SIGNAL(Recv(int)),this,SLOT(Sign(int)));
    connected=false;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
   if(!connected)
   if(st->Connect(ui->lineEdit->text(),ui->lineEdit_2->text().toInt()))
      {
          st->start();
          connected=true;
      }

}

void MainWindow::on_pushButton_2_clicked()
{
    if(connected)
    {
        QString ar1=ui->textEdit->toPlainText();
        QString na1=ui->lineEdit_3->text();
        const QByteArray ar=ar1.toUtf8();
        const QByteArray na=na1.toUtf8();
        st->Send(na);
        st->Send(":\n");
        st->Send(ar);
    }
}

void MainWindow::Sign(int i)
{
    QMutex m;
    m.lock();
    QTextCursor cursor=ui->textBrowser->textCursor();
    ui->textBrowser->insertPlainText(st->buff);
    ui->textBrowser->insertPlainText("\n");
    cursor.movePosition(QTextCursor::End);
    ui->textBrowser->setTextCursor(cursor);
    m.unlock();
}





void MainWindow::on_pushButton_3_clicked()
{
    if (connected)
        {
        st->Disconnect();
        connected=false;
        }

}
