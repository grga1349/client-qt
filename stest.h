#ifndef STEST_H
#define STEST_H

#include <QObject>
#include <QtNetwork/QTcpSocket>
#include <QDebug>
#include <QtCore>


class sTest : public QThread
{
    Q_OBJECT
public:
    explicit sTest (QObject *parent=0);
    bool Connect(QString,int);
    void Disconnect();
    void Send(const QByteArray &);
    void run();
    QString buff;


private:
    QTcpSocket *socket;
    bool TRun;

signals:
    void Recv(int);

public slots:
};

#endif // STEST_H
