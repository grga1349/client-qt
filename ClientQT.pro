#-------------------------------------------------
#
# Project created by QtCreator 2016-05-08T23:16:23
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClientQT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    stest.cpp

HEADERS  += mainwindow.h \
    stest.h

FORMS    += mainwindow.ui
